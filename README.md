# HID_Firmware_Upgrade

## 介绍
 基于 QT 的 USB-HID 固件 升级软件以及 USB 调试软件 ，使用私有协议进行文件的传输以及校验

## 功能

1. bin 文件的读取、显示、按协议下载
2. USB 通讯数据的实时显示
3. 文件路径记忆、参数可配置和记忆
4. 实时的插拔检测
5. 在线上传和下载固件（需要服务器支持）


## 计划中功能

- USB波形的显示

<p align="center">
<img src="http://www.whtiaotu.com/picture/HID_Firmware_Upgrade/HID_Firmware_Upgrade1.gif">
</p>

<p align="center">
<img src="http://www.whtiaotu.com/picture/HID_Firmware_Upgrade/HID_Firmware_Upgrade2.png">
</p>


## 软件架构
软件架构说明

基于 QT 调用 WIN 的 hidapi 实现 USB 的通信，下位机是 STM32 HAL 库的USB

QT 版本：5.9.0

编译器：mingw

支持平台：Windows


## 使用说明

1.  下载 Bootloader 到单片机，用 USB 线连接单片机与电脑，然后点击开始下载即可

## 注意事项

1.  程序运行需要 msvcr100d.dll 和 hidapi.dll 支持，这两个文件在源码中有，有些 windows 携带了msvcr100d.dll 有的没有，使用时请注意确保这两个文件在 .exe 的同级目录，打包的时候也要带上
2.  如果在 USB 连接状态下关闭软件出现无响应，可以尝试复位 MCU 或者断开 USB 线连接
4.  程序可以显示 bin 文件和 USB 通讯文件到缓冲区，这是调试用的功能，实际使用的时候会耽误效率

## 下载源码

`git clone https://gitee.com/mzy2364/HID_Firmware_Upgrade.git`

## 编译

删除 HID_Firmware_Upgrade.pro.user，用 Qt Creator 打开 HID_Firmware_Upgrade.pro 配置一下程序输出地址然后编译运行

## 已经支持的下位机目标板

Bison-Board 野牛开发板

NUCLEO-144 F767 ST 官方开发板

ebf_f429_v1 野火 F429V1 核心板