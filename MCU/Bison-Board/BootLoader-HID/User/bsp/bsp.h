/*
*******************************************************************************************************
*
* 文件名称 : bsp.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : BSP头文件,该文件包含所有需要的头文件,其余文件只需包含这一个头文件即可
* 
*******************************************************************************************************
*/

#ifndef _BSP_H
#define _BSP_H


/* 头文件 -----------------------------------------------------------*/
/*C library*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*STM32&CMSIS*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_conf.h"


/*BSP*/
#include "led.h"
#include "usart.h"
#include "key_fifo.h"
#include "fmc.h"
#include "iap.h"
#include "ltdc.h"
#include "delay.h"
#include "wks.h"

/*USB*/
#include "usb_device.h"


/*GUI*/


/*RTOS*/


/*FATFS*/




/* 宏定义 -----------------------------------------------------------*/


#define BOOTLOADER_VERSION "V1.0"		//固件版本

/*时钟选择*/
//#define CLOCK_240M
#ifndef CLOCK_240M
#define CLOCK_192M
#endif





/* 全局变量 ----------------------------------------------------------*/


/* 函数声明 ----------------------------------------------------------*/

/* 外部调用函数 */
void bsp_init(void);
void bsp_tick(void);
void print_logo(void);


/* 仅内部调用函数 */
uint32_t bsp_get_tick(void);
void Error_Handler(void);
void SystemClock_Config(void);
void bsp_tim_init(void);

#endif

/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
