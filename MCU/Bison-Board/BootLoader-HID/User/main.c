/*
*******************************************************************************************************
*
* 文件名称 : main.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 主函数文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"
#include "flash_if.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/
extern pFunction JumpToApplication;


/**
  * @brief main主函数
  * @param None
  * @retval	None
  */
int main(void)
{
	
	uint32_t i=0;
	uint32_t temp;
	
	bsp_init();
	
	key_scan();
	
	/* 延时等待按键滤波,为了不动BSP,这里继续使用原来的FIFO按键 */
	HAL_Delay(60);
	
//	for(i=0;i<10;i++)
//	{
//		if((i%4)==0)
//			printf("\r\n");
//		temp = flash_if_read_word(APPLICATION_ADDRESS+4*i);
//		printf("%02x%02x %02x%02x ",(uint8_t)temp,(uint8_t)(temp>>8),(uint8_t)(temp>>16),(uint8_t)(temp>>24));
//	}
	
	if(key_get() == KEY_LEFT_DOWN)
	{
		/* 按键按下了,进入IAP */
		iap_schedule();
	}
	else
	{
		
		/* Test if user code is programmed starting from address "APPLICATION_ADDRESS" */
		if (((*(__IO uint32_t*)APPLICATION_ADDRESS) & 0x2FF00000  ) == 0x20000000)
		{
			
			/* 关闭外设 */
			HAL_DeInit();
			HAL_RCC_DeInit();
			
			
			
			/* 跳转前关闭用户开启的所有中断 */
			HAL_NVIC_DisableIRQ(TIM4_IRQn);
			HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
			HAL_NVIC_DisableIRQ(SysTick_IRQn);
			
			/* Jump to user application */
			JumpToApplication = (pFunction) *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
			/* Initialize user application's Stack Pointer */
			__set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
			JumpToApplication();
		}
		
	}
	
	/* 跳转到APP失败,进入IAP */
	iap_schedule();
	
	
	
	while(1)
	{
		HAL_Delay(10);
	}
	
	
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
