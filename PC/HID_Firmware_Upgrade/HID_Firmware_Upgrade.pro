#-------------------------------------------------
#
# Project created by QtCreator 2018-11-21T13:29:23
#
#-------------------------------------------------

QT       += core gui
QT += widgets axcontainer


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HID_Firmware_Upgrade
TEMPLATE = app

RC_ICONS = usb_hid.ico

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    usbthread.cpp \
    setting.cpp \
    dialog_set.cpp \
    dialog_help.cpp \
    webbrowser.cpp
HEADERS += \
        mainwindow.h \
    hidapi.h \
    usbthread.h \
    setting.h \
    dialog_set.h \
    dialog_help.h \
    webbrowser.h

FORMS += \
        mainwindow.ui \
    dialog_set.ui \
    dialog_help.ui \
    webbrowser.ui

LIBS += -L$$_PRO_FILE_PWD_/  -lhidapi

RESOURCES += \
    img.qrc
