/********************************************************************************
** Form generated from reading UI file 'webbrowser.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEBBROWSER_H
#define UI_WEBBROWSER_H

#include <ActiveQt/QAxWidget>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WebBrowser
{
public:
    QGridLayout *gridLayout;
    QAxWidget *Web_Browser;

    void setupUi(QWidget *WebBrowser)
    {
        if (WebBrowser->objectName().isEmpty())
            WebBrowser->setObjectName(QStringLiteral("WebBrowser"));
        WebBrowser->resize(400, 300);
        gridLayout = new QGridLayout(WebBrowser);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(5, 5, 5, 5);
        Web_Browser = new QAxWidget(WebBrowser);
        Web_Browser->setObjectName(QStringLiteral("Web_Browser"));

        gridLayout->addWidget(Web_Browser, 0, 0, 1, 1);


        retranslateUi(WebBrowser);

        QMetaObject::connectSlotsByName(WebBrowser);
    } // setupUi

    void retranslateUi(QWidget *WebBrowser)
    {
        WebBrowser->setWindowTitle(QApplication::translate("WebBrowser", "Form", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WebBrowser: public Ui_WebBrowser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEBBROWSER_H
